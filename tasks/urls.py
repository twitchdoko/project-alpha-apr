from django.urls import path
from tasks.views import create_task, viewtasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", viewtasks, name="show_my_tasks"),
]
